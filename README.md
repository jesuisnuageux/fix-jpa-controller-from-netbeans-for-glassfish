# How to use this script

- copy it in the folder where your jpa controllers are
- then you can fix one specific controller by passing its filename as parameter 
- or you can just fix every controllers in the current directory by running the script without parameter

Explicitly, it looks like this :

> cp ./fixthem.py _{path to your jpa controller folder}_/


> ./fixthem UserJpaController.java # will fix this one only

or 

> ./fixthem.py # will fix all of them :D 

*NB : this script provide working code, not good looking one. I didn't wanted to spend more time tweaking it then I would fixing controllers by hand*

