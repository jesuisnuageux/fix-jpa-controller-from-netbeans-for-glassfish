#!/usr/bin/python

import re
import sys
import os


data = [
	{
		"regex":"(\ )+public [a-zA-Z]+JpaController(.|\n)+emf\.createEntityManager\(\);\n(\ )+\}",
		"new_value":"@Resource\n    private UserTransaction utx;\n\n   @PersistenceContext\n    private EntityManager em;"
	},
	{
		"regex":"EntityManager\ em\ =\ null;",
		"new_value":""
	},
	{
		"regex":"EntityManager\ em\ =\ getEntityManager\(\);",
		"new_value":""
	},
	{
		"regex":"em\ =\ getEntityManager\(\);",
		"new_value":""
	},
	{
		"regex":"em\.close\(\)\;",
		"new_value":"// LOL GROS PATCH RAPIDE DES FAMILLES // em.close();"
	},
	{
		"regex":"\*\/\npublic class",
		"new_value":"*/\n@Named\n@RequestScoped\npublic class"
	},
	{
		"regex":"import\ javax\.persistence\.EntityManagerFactory;",
		"new_value":"import javax.annotation.Resource;\nimport javax.persistence.PersistenceContext;\nimport javax.enterprise.context.RequestScoped;\nimport javax.inject.Named;"
	}
]

def fix_file(filename):
	with open(filename, "rw") as f:
		content = '\n'.join([ line.rstrip() for line in f.readlines()])

		print "Content length : {}".format(len(content))

		for i in range(len(data)):
			content = re.sub(data[i]['regex'], data[i]['new_value'], content)
			print "Content length : {}".format(len(content))

	with open(filename, "w") as f:
		f.write(content)


if __name__ == '__main__':

	narg = len(sys.argv)
	if narg == 2:

		if not os.path.exists(sys.argv[1]):
			print "I couldn't find this file, s'rry bro :/"
			sys.exit()

		fix_file(sys.argv[1])

	else:
		for file in os.listdir(os.getcwd()):
			if file.endswith("JpaController.java"):
				print "Treating {}".format(file)
				fix_file(file)
